const mongoose = require("mongoose");
const Order = require("../models/orders.js");
const User = require("../controllers/userController.js");
const Product = require("../models/products.js");


module.exports.addOrder = async (reqBody) => {
    const product = await Product.findOne({ _id: reqBody.productId }).then((result, error) => {
        if (error) {
            return null;
        }
        return result;
    });

    if (product === null) {
        return false;
    }

    const newOrder = new Order({
        userId: reqBody.userId,
        products: [
            {
                productId: reqBody.productId,
                quantity: reqBody.quantity,
            },
        ],
        totalAmount: reqBody.quantity * product.price,
    });

    return newOrder.save().then((result, error) => {
        if (error) {
            return false;
        }
        return result;
    });
};


